# Contribute to MMMCore by filing issues
---

If you think MMMCore is not working as expected please contact the developers as
indicated in README.md or check the issue tracker if the problem has already been
reported.

Before reporting a yet unknown issue, try to reduce the issue to the bare minimum required to reproduce it.
This allows us to track down and fix the issue in an easier and faster way.

Additionally, you should give us enough information to reproduce the issue.
Therefore, include versions of your OS, MMMCore (git id, release), CMake, Boost,
and other used libraries as well as sample code.
If you don't list the exact steps required to reproduce the issue we won't be able to fix it.

Afterwards, report the issue on the following tracker (GitLab account required):

* [MMMCore Issues](https://gitlab.com/mastermotormap/mmmcore/issues)


# Developing MMMCore
---

If you want to get started developing MMMCore and contributing patches,
there are a few things required to get you started:

* GitLab account
* Git (to get a clone of the repository)
* CMake (to build MMMCore)
* Boost
* Eigen3
* Doxygen (optional for generating Documentation)


## Setup of your MMMCore clone

To contribute to MMMCore you need to use the SSH link for cloning the repository
which is described next.
If you already have a clone of MMMCore, skip the next steps
for cloning MMMCore and refer to [Update MMMCore repository URL](#update-mmmcore-repository-url).


1. Clone the repository:
 * `cd ~`
 * `git clone git@gitlab.com:mastermotormap/mmmcore.git MMMCore`
2. Build MMMCore:
 * `mkdir build`
 * `cmake ..`
 * `make`


## Update MMMCore repository URL 

If you want to contribute to the development of MMMCore you need to use
the SSH repository link of GitLab.
Please note that a working GitLab account which is configured with an SSH key
is required for this step to succeed.

1. `cd ~/MMMCore`
2. `git remote set-url origin git@gitlab.com:mastermotormap/mmmcore.git`


## Submitting patches

The MMMCore development model for external contributors currently revolves
around Merge Requests which are created through GitLab.

### Prepare repository for a Merge Request


1. Login to Gitlab and go to https://gitlab.com/mastermotormap/mmmcore
2. Click on the `fork` button on the right side (this will create your own copy on Gitlab)
3. Copy the repository URL from the SSH field of your repository page (you were redirected to this page after the fork)
4. Add the URL of your fork to your local repository
 * `git remote add gitlab-fork <your gitlab fork url>`
 * Of course, you can use a different name than `gitlab-fork`. In this case you will need to replace `gitlab-fork` with this name, too, when pushing to your fork.


### Create a Merge Request

1. Update to latest MMMCore master (`git pull`)
2. If possible, develop your feature or bugfix in a local branch (not in `master`)
3. If possible, create unittest for your feature or bugfix (your feature/fix will be integrated a lot faster if unittests are present)
4. Enhance/fix MMMCore
5. Run the unittests
6. Commit your changes to your local repository if all tests are green
7. Try to split your fix into small Git commits if multiple changes are involved (this makes it easier for us to review the changes)
8. Push the changes to your fork on gitlab `git push gitlab-fork <your branchname>`
9. Submit the Merge Request (usually to the MMMCore master branch) by performing the following steps
 * Go to the GitLab page of your MMMCore fork (https://gitlab.com/<your gitlab username>/mmmcore/)
 * Click on the menu item `Merge Requests`
 * Click on the green button `New Merge Request`
 * In the box `source branch` select the branch to merge into the MMMCore master repository
 * Click on `Compare Branches`
 * Add a title and a description of your work to be merged
 * Finally click the green button `Submit merge request`
 * Wait for the developers to merge your request
