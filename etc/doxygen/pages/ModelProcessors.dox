/** 
\page modelprocessors The MMM Model Processors

Model Processors are used to adapt the generic \ref models "MMM Models" to a set of parameters that specify the appearance of the proband. 
Details on how a custom model processor can be implemented are given here: \ref MMM::ModelProcessorFactory.

<!--\section mmm-model-processor-general The Model Processor Framework-->


\section mmm-model-processor-winter The Winter Model Processor

The MMM::ModelProcessorWinter class can be used to scale a generic MMM reference model (given with a standard height of 1 m) to a proband specific height. Scaling is
performed according to Winter's biomechanic definitions (see \ref models). The setup can be given via an XML config file. 


\subsection mmm-model-processor-winter-setup Setup of the Winter Model Processor
An exempary usage of the Winter ModelProcessor is given below.
 
\code
// Example on how to create an instance of the Winter model processor

#include <MMM/Model/ModelProcessorWinter.h>

// ...

// retrieve model processor factory by name
std::string modelProcessorType("Winter");
MMM::ModelProcessorFactoryPtr modelFactory = MMM::ModelProcessorFactory::fromName(modelProcessorType, NULL);
if (!modelFactory)
{
   cout << "Could not create model processing factory of type " << modelProcessorType << endl;
   cout << "Setting up model processor... Failed..." << endl;
   return;
}

// create model processor instance
MMM::ModelProcessorPtr modelProcessor = modelFactory->createModelProcessor();

// setup model processor from file
std::string modelProcessorFile("../data/ModelProcessor_Winter_1.70.xml");
if (!modelProcessor->setupFile(modelProcessorFile))
{
   cout << "Error while configuring model processor '" << modelProcessorType << "' from file " << modelProcessorFile << endl;
   cout << "Setting up model processor... Failed..." << endl;
}
cout << "Setting up model processor... OK..." << endl;

\endcode

\subsection mmm-model-processor-winter-usage Usage of the Winter Model Processor

The model processor can be used as follows:
\code

// load a model
std::string modelFile("../data/Model/Winter/Winter.xml");
MMM::ModelReaderXMLPtr r(new MMM::ModelReaderXML());
MMM::ModelPtr mmmOrigModel = r->loadModel(modelFile);

// apply a processor specific setup (optional)
MMM::ModelProcessorWinterPtr mpWinter = boost::dynamic_pointer_cast<MMM::ModelProcessorWinter>(modelProcessor);
// The config file setup can be overwritten (Proband with 1.8m and 90kg):
mpWinter->setup(1.80f,90.0f);

// create a scaled model
MMM::ModelPtr mmmModel = modelProcessor->convertModel(mmmOrigModel);

\endcode

\subsection mmm-model-processor-winter-config Configuration file of the Winter Model Processor

An exemplary configuration file is shown here:
\code
<ModelProcessorConfig type='Winter'>
    <height>1.70</height>
    <mass>75</mass>
</ModelProcessorConfig>
\endcode

\subsection mmm-model-processor-winter-custom-seg-lengths Specifying custom segment lengths with the Winter Model Processor

The segment lengths can be set by hand in order to be able to increase the accuracy by adding subject specific data. The given segment will be scaled to the provided value.
\code
<ModelProcessorConfig type='Winter'>
    <height>1.70</height>
    <mass>75</mass>
    <SegmentLength name='LAsegment_joint' unit='m'>0.8</SegmentLength>
    <SegmentLength name='RAsegment_joint' unit='m'>0.8</SegmentLength>
</ModelProcessorConfig>
\endcode

*/
