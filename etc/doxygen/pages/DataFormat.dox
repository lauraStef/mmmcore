/** 
\page dataformat The MMM Data Format

The MMM data format is an extendable XML-based format for storing model-based motions.

\section basic_data_format The Basic MMM Motion Data Format

The main tag which encapsulates the MMM related motion data is \<MMM\> ... \</MMM\>. Within these tags a motion is addressed via the \<Motion name='my name'\> ... \</Motion\> XML structure.
Inside a motion, several top level XML tags are available by default which can be extended in order 
to meet custom demands (see \ref extensions "Extending the MMM Data Format"). 
The top level motion tags cover \em \<Comments\>, \em \<JointOrder\>, \em \<Model\> and \em \<ModelProcessorConfig\> tags. 

\subsection mmm_data_comments The \<Comments\> tag
Custom text can be stored within the comments tag. Therefore the text must be encapsulated by \em \<text\> \em \</text\> tags.\n
Here is an example:
\code
<Motion name = 'test'>
	<Comments>
		<text>First line of text</text>
		<text>Second line of text</text>
	</Comments>
</Motion>
\endcode


\subsection mmm__model_file The \<Model\> tag
The motion can be optionally linked to an MMM Model. Therefore the filename of the MMM Model can be specified here.\n
Here is an example:
\code
<Motion name = 'test'>
  <Model>
	<File>/home/dev/models/Winter.xml</File>
  </Model>
</Motion>
\endcode


\subsection mmm_data_modelproecssor_config The \<ModelProcessorConfig\> tag
The model can be processed in order to change its parameters (see \ref modelprocessors "The MMM Model Processors"). This tag specifies a model processor and serves configuration paramters for it.
The type attribute specifies which model processor has to be used. By using the factory layout, the corresponding model processor is automatically selected while loading the motion.
Internally the motion loader uses this processor for processing the input model according to the given paramteres. Note that this tag is optional, i.e. without it the 
original model will be used for further processing.\n
Here is an example:
\code
<Motion name = 'test'>
	<ModelProcessorConfig type='Winter'>
		<Height>1.7</Height>
		<Mass>75</Mass>
	</ModelProcessorConfig>
</Motion>
\endcode


\subsection mmm_data_joint_order The \<JointOrder\> tag

The joint order is specified with the \em \<JointOrder\> tag. 
The joint order is later used to identify which joints are addressed by the MotionData information.\n
Here is an example:
\code
<Motion name = 'test'>
	<JointOrder>
		<joint name='BTx_joint'/>
		<joint name='BTy_joint'/>
		<joint name='BTz_joint'/>
		...
	</JointOrder>
</Motion>
\endcode



\subsection mmm_data_motion The \<MotionFrames\> tag

The motion of an MMM model is defined with the \em \<MotionFrames\> tag by specifying \em \<MotionFrame\> tags for discrete timesteps.
A basic MotionFrame entry is filled with the following information:
- timestep: The time stamp (given in ms)
- RootPosition: The position of the model's root coordinate system at the given timestep (millimeters).
- RootPositionVelocity: The velocity of the model's root coordinate system at the given timestep (optional).
- RootPositionAcceleration: The acceleration of the model's root coordinate system at the given timestep (optional).
- RootRotation: The orientation of the model's root coordinate system at the given timestep (Roll Pitch Yaw angles).
- RootRotationVelocity: The orientation velocity of the model's root coordinate system at the given timestep (optional).
- RootRotationAcceleration: The orientation acceleration of the model's root coordinate system at the given timestep (optional).
- JointPosition: The joint values for the joint set as defined by the <JointOrder> header (radian).
- JointVelocity: The joint velocities for the joint set as defined by the <JointOrder> header.
- JointAcceleration: The joint accelerations for the joint set as defined by the <JointOrder> header.

Here is an example:
\code
<MMM>
	<Motion>
		<JointOrder>
			...
		</JointOrder>
		<MotionFrames>
			<MotionFrame>
				<Timestep>0</Timestep>
				<RootPosition>61.6203 -582.555 855.711</RootPosition>
				<RootRotation>-0.120688 0.0474702 -0.0781915</RootRotation>
				<JointPosition>-0.217267 0.267692 -0.85853 0.718723 -0.795766 0.517872 -0.148897 0 0</JointPosition>
				<JointVelocity>0 0 0 0 0 0 0 0 0</JointVelocity>
				<JointAcceleration>0 0 0 0 0 0 0 0 0</JointAcceleration>
			</MotionFrame>
			<MotionFrame>
				...
			</MotionFrame>
			...
		</MotionFrames>
	</Motion>
</MMM>
\endcode

\section motion_api Accessing the MMM Motion Data

The MMMCore library offers several convenient methods to load motion data files. They can be loaded by using the MMM::MotionReaderXML utility class.

\code
// instaciate a motion reader
MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML());

// get all motion names from file
std::vector < std::string > motions = motionReader->getMotionNames("motion.xml");

// load first motion
if (motions.size()>0)
{
	MMM::MotionPtr motion = motionReader->loadMotion("motion.xml",motions[0]);

	// get the associated model (this is the processed model)
	MMM::ModelPtr mmmModel = motion->getModel();

	// get names of the involved joints
	std::vector<std::string> jointNames = motion->getJointNames();

	//access first motion frame
	MMM::MotionFramePtr motionFrame = motion->getMotionFrame(0);

	// access the root pose of this motion frame
	Eigen::Matrix4f rootPose = motionFrame->getRootPose();
}
\endcode

*/