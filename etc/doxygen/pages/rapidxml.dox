/** 
\page rapidxml Rapid XML

Rapid XML is a very fast header only C++ XML reader which is used within 
the MMM framework. 
The basic syntax is as follows:

\section xmlDocs process XML string

\code

// include MMM XMLTools
#include "XMLTools.h"

// include rapidxml
#include "rapidxml.hpp"

void myXMLParseMethod(std::string &xmlString)
{
	try
	{
		rapidxml::xml_document<char> doc; // character type defaults to char
		char* y = doc.allocate_string(xmlString.c_str()); // the memory is automatically deleted when doc dies
		doc.parse<0>(y);                  // 0 means default parse flags

		// get first tag
		rapidxml::xml_node<>* node = doc.first_node();
		while (node)
		{
			std::string nodeName = XML::getLowerCase(node->name());
			
			// process xml tag with name nodeName, e.g.
			if (nodeName == "myCustomTag")
				processTag(node);
			
			// get next tag
			node = node->next_sibling();
		}
	}
	catch (rapidxml::parse_error& e)
	{
		// some error processing
	}
}
\endcode

\section xmlAttr Access XML hierarchy and attributes

\code
	
bool processTag(rapidxml::xml_node<char>* tag)
{
	// get an attribute
	rapidxml::xml_attribute<> *attr = tag->first_attribute("type", 0, false);
	if (attr)
	{
		// process attribute as float
		float f = XML::convertToFloat(attr->value());
		
		// process string attribute 
		std::string s = attr->value();
	}
	
	// get second level tag hierarchy
	rapidxml::xml_node<>* node = tag->first_node();
	while (node)
	{
		std::string nodeName = XML::getLowerCase(node->name());
		
		// process second level tags ...
		
		node = node->next_sibling();
	}
}
\endcode

*/
