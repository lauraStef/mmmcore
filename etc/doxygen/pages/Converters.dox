/** 
\page converters The MMM Converter Framework

The MMM converter framework offers methods and tools that allow 
to realize generic as well as model specific converter classes. 

Custom converters can be implemented based on the two classes MMM::Converter and MMM::ConverterFactory.

Exemplary converter implementations are provided with the \ref mmmtools "MMM Tools" package.

\section implementing_converters Implementing Converters

The class MMM::Converter offers some basic functionality that can be 
extended for building a converter within the MMM framework. Any 
derived classes msut impelement the following methods:
 - virtual AbstractMotionPtr convertMotion();<br>
   With this method a complete motion should be converted (see 
   MMM::Converter for details on setting up a Converter)
   
  - virtual AbstractMotionPtr initializeStepwiseConvertion();<br>
    Some converters may allow stepwise processing of the input motion. 
    This mode is initialized here.

  - virtual bool convertMotionStep(AbstractMotionPtr currentOutput);<br>
    Compute next step of currentOutput (when in stepwise mode).

  - virtual bool _setup(rapidxml::xml_node<char>* rootTag);<br>
	This private method is used to initialize the Converter with XML 
	definitions. By default, the Converter class offers convenient 
	methods to pass filenames or XML strings wich are processed by the 
	\ref rapidxml toolbox. Internally the _setup method is called in order to allow custom tag processing.

\section implementing_converter_factories Converter Factories 
The ConverterFactory can be used to make custom converters accessible by the MMM framework. 
If converters are implemented with the proposed structure, the \ref mmmtools \ref mmmconverter 
and \ref mmmconvertergui tools are capable of using these converteres in order to process motions.
	
	An exemplary implementation may look like this
	
	\code

	// The header MyCustomConverterFactory.h

	namespace MMM 
	{

	class MMM_IMPORT_EXPORT MyCustomConverterFactory  : public ConverterFactory
	{
	public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

		MyConverterFactory() ;

		virtual ~MyConverterFactory();

		virtual ConverterPtr createConverter();

		static std::string getName() {return "MyCustomConverterFactory"};

		static boost::shared_ptr<ConverterFactory> createInstance(void*);

	private:
		static SubClassRegistry registry;
	};
	
	typedef boost::shared_ptr<MyCustomConverterFactory> MyCustomConverterFactoryPtr;

	}
	\endcode

	\code
	
	// The source MyCustomConverterFactory.cpp
	
	#include "MyCustomConverterFactory.h"
	#include <boost/extension/shared_library.hpp>
	#include <boost/function.hpp>

	namespace MMM
	{

	// register this factory, so that is accessible within the API
	ConverterFactory::SubClassRegistry MyCustomConverterFactory::registry(MyCustomConverterFactory::getName(), &MyCustomConverterFactory::createInstance);

	MyConverterFactory::MyConverterFactory()
	{
		// some init code
	}
	MyConverterFactory::~MyConverterFactory()
	{
		// some exit code
	}

	ConverterPtr MyConverterFactory::createConverter()
	{
		ConverterPtr converter(new MyCustomConverter());
		return converter;
	}

	boost::shared_ptr<ConverterFactory> MyConverterFactory::createInstance(void*)
	{
		boost::shared_ptr<ConverterFactory> converterFactory(new MyCustomConverterFactory());
		return converterFactory;
	}
	
	}

	// Enable Plugin mechanism, that allows the MMMConverter tool to load this converter
	// The MMMTools::MMMConverter expects a getFactory method which is used to initialize the custom converter factory
	extern "C"
	BOOST_EXTENSION_EXPORT_DECL MMM::ConverterFactoryPtr getFactory() {
		MMM::MyConverterFactoryPtr f(new MMM::MyConverterFactory());
		return f;
	}
	\endcode

*/
