 /*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Nikolaus Vahrenkamp
* @copyright  2013 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/
#ifndef __MMM_Model_H_
#define __MMM_Model_H_

#include "../MMMCore.h"
#include "../MMMImportExport.h"
#include "../XMLTools.h"
#include "ModelNode.h"

#include <Eigen/Core>
#include <string> 
#include <vector>
#include <map>

#include <boost/foreach.hpp>


namespace MMM
{

struct ModelNodeSet
{
    std::string ModelNodeSetName;
    std::string rootName;
    std::string tcpName;

    std::vector<std::string> ModelNodes;
};

class Model;
typedef boost::shared_ptr<Model> ModelPtr;

/*!
	@brief A data structure that defines an MMM model.
	The model consists of ModelNodes that form a kinematic tree.
	@see ModelReader
*/
class MMM_IMPORT_EXPORT Model
{
	friend class ModelReaderXML;
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW


	Model();
	/*!
		Constructor
		@param type The model type
		@param name The name od this model. Used to distinguish between different models of same type.
	*/
	Model(const std::string &type, const std::string &name);

	bool addModelNode(ModelNodePtr n);
    bool addModelNodeSet(ModelNodeSet ns);


	void setName(const std::string &name);
	void setType(const std::string &type);

	//! Returns name of this model.
	std::string getName();

	//! Returns type of this model.
	std::string getType();

	void setRoot(const std::string &name);

    //! Returns name of root node.
	std::string getRoot();

	void setMass(float massKG);
	void setHeight(float heightM);

	float getHeight();
	float getMass();

    //! Returns all nodes.
	std::vector<ModelNodePtr> getModels();

    std::vector<ModelNodeSet> getModelNodeSets();

    ModelNodePtr getModelNode(const std::string& nodeName);

	//! Checks if a marker with given name is present in the model
	bool hasMarker(const std::string &markerName) const;

    //! Create an XML string that describes this model.
	std::string toXML();

	void print();

	ModelPtr clone();

	std::string getFilename();

	std::string filename; //! When read from file, the name is stored here.

protected:

	std::vector<ModelNodePtr> modelNodes;
	std::string rootNode;
	std::string name;
	std::string type;
	float mass;			//<! kg
	float height;		//<! meter
    std::vector<ModelNodeSet> modelNodeSets;

};


}

#endif 
