#include <Eigen/Core>
#include <unsupported/Eigen/AutoDiff>
#include <unsupported/Eigen/Splines>
#include "MMMCore.h"
#include "splinefunction.h"

namespace MMM {

SplineFunction::SplineFunction(Eigen::VectorXf input,int p)
{   int len_input = input.rows();
    Eigen::VectorXf times(len_input);
    for(int i = 0; i< len_input; i++){
        times(i) = float(i)/len_input;  // inits times and scales times to [0,1]
    }

    Eigen::MatrixXf timesVector = times.transpose();
    this->derivative = Eigen::VectorXf::Zero(len_input);
    this->derivative2ndOrder = Eigen::VectorXf::Zero(len_input);
    Eigen::MatrixXf input1 = 100. *input.transpose()/len_input;
    ControlPointVectorType points = input1;
    const spline s = Eigen::SplineFitting<spline>::Interpolate(points,3,timesVector);

    for (int time = 0; time < len_input; time++) {
        float point = float(time)/len_input;// chord_lengths(time); //transforms to [0,1]
        auto y = s.derivatives<2>(point);
        this->derivative(time) = y(0,1);
        this->derivative2ndOrder(time) = y(0,2);
    }

}

SplineFunction::SplineFunction(Eigen::VectorXf input) {
    int len_input = input.rows();

    this->derivative = Eigen::VectorXf::Zero(len_input);
    this->derivative2ndOrder = Eigen::VectorXf::Zero(len_input);
    alglib::real_1d_array data_part,x_points_part;
    alglib::ae_int_t m = 3;
    alglib::ae_int_t info;
    alglib::barycentricinterpolant p;
    alglib::polynomialfitreport rep;

    int window = 2*26;
    for(int j = window/2;j< len_input-window/2; j++){

        data_part.setlength(window);
        x_points_part.setlength(window);

        for(int i = 0; i< window; i++){
            data_part(i) = input(j-window/2+i)*100;  // inits data and scales data to 100Hz
            x_points_part(i) = i;
        }

        alglib::polynomialfit(x_points_part,data_part,m,info,p,rep);

            double der,f, der2;
            alglib::barycentricdiff2(p,double(window/2),f,der,der2);
            derivative(j)  = der;
            derivative2ndOrder(j) = der2*100;

    }

//    /*set values smaller and bigger window with first window*/
//    data_part.setlength(window/2);
//    x_points_part.setlength(window/2);
//    m=2;
//    for(int i = 0; i< window/2; i++){
//        data_part(i) = input(i)*100;  // inits data and scales data to 100Hz
//        x_points_part(i) = i;
//    }
//    alglib::polynomialfit(x_points_part,data_part,m,info,p,rep);

//    for(int j = 0;j< window/2; j++){
//            double der,f, der2;
//            alglib::barycentricdiff2(p,double(j),f,der,der2);
//            derivative(j)  = der;
//            derivative2ndOrder(j) = der2*100;
//    }

//    /*bigger len - window */
//    data_part.setlength(window);
//    x_points_part.setlength(window);

//    for(int i = 0; i< window; i++) {
//        data_part(i) = input(len_input-window +1 +i)*100;  // inits data and scales data to 100Hz
//        x_points_part(i) = i;
//    }

//    alglib::polynomialfit(x_points_part,data_part,m,info,p,rep);

//    for(int j = len_input-window +1;j< len_input; j++){
//            double der,f, der2;
//            alglib::barycentricdiff2(p,double(j),f,der,der2);
//            derivative(len_input-window +1 + j)  = der;
//            derivative2ndOrder(len_input-window +1 +j) = der2*100;

//    }
}
}


