#ifndef __MMM_MOTIONFRAMEZMPNECESSITIES_H
#define __MMM_MOTIONFRAMEZMPNECESSITIES_H

#include "../MMMCore.h"
#include "../MMMImportExport.h"
#include "MMM/MathTools.h"


#include <Eigen/Core>
#include<Eigen/Geometry>
#include<Eigen/Dense>
#include <fstream>

namespace MMM{
class MotionFrameZmpNecessities;
/**
    @brief A Motion defines a series of MotionFrame snapshots.
    The data covers position, velocity and acceleration of the models root position and orientation
    and a set of joint values, velocities and accelerations.
*/
typedef boost::shared_ptr<MotionFrameZmpNecessities> MotionFrameZmpNecessitiesPtr;

class MMM_IMPORT_EXPORT MotionFrameZmpNecessities
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    MotionFrameZmpNecessities();

    MotionFrameZmpNecessities(const MotionFrameZmpNecessities &inpSrc);

    MotionFrameZmpNecessitiesPtr copy(){
        MotionFrameZmpNecessitiesPtr  res(new MotionFrameZmpNecessities());
        res->angular_momentum = this->angular_momentum;
        res->angular_velocity_segments = this->angular_velocity_segments;
        res->com = this->com;
        res->number_of_relevant_segments = this->number_of_relevant_segments;
        res->segment_COM = this->segment_COM;
        res->segment_COM_derivative = this->segment_COM_derivative;
        res->rotation_matrix_segments = this->rotation_matrix_segments;
        res->inertia_tensor_list = this->inertia_tensor_list;
        return res;

    }

    MotionFrameZmpNecessitiesPtr handOver();

    bool setNumberOfRelevantSegments(int seg_number);


    int number_of_relevant_segments;
    Eigen::VectorXf segment_COM;
    Eigen::VectorXf segment_COM_derivative;
    Eigen::VectorXf segment_COM_acceleration;
    Eigen::Vector3f com;
    Eigen::Vector3f com_derivative;
    Eigen::Vector3f com_acceleration;
    Eigen::VectorXf angular_velocity_segments;
    Eigen::VectorXf angular_velocity_segments_derivative;
    Eigen::VectorXf angular_pos_segments;
    Eigen::VectorXf angular_pos_segments_derivative;
    Eigen::VectorXf angular_pos_segments_2nd_derivative;
    Eigen::VectorXf segment_mass;
    Eigen::VectorXf angular_momentum_derivative;
    Eigen::VectorXf angular_momentum;
    std::vector<Eigen::Matrix3f> rotation_matrix_segments;
    std::vector<Eigen::Matrix3f> rotation_matrix_segments_derivative;
    std::vector<Eigen::Matrix3f> inertia_tensor_list;

    float dotProduct(Eigen::Vector3f firstVec, Eigen::Vector3f secondVec);

    bool setRotationMatrix(Eigen::Matrix4f rootPose, Eigen::Matrix4f rotations, int pos);

    bool calculateAngularVel(Eigen::Matrix4f poseToRoot, int i);

    bool setCOM(Eigen::Vector3f com, Eigen::Matrix4f globalPose);

    void setAngularMomentum(std::vector<Eigen::Matrix3f> inertia_tensors);

    void setSegmentMasses(std::vector<float> segment_masses);

    void setAngularVelViaRotationMatrix();

    bool setSegmentsCOM(Eigen::Matrix4f rootPose, Eigen::Vector3f com, int segment);

    void setAngularPos(Eigen::Matrix4f pose, int pos);
    Eigen::Matrix4f rescaleToRootPose(Eigen::Matrix4f rootPose);


};
}
#endif // MOTIONFRAMEZMPNECESSITIES_H
