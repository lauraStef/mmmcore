
#include "MotionFrame.h"

#include "../XMLTools.h"
#include "../MathTools.h"

namespace MMM
{

    MotionFrame::MotionFrame(unsigned int ndof)
    {
        this->ndof = ndof;
        timestep = 0.0f;
        zmp = Eigen::Vector2f::Zero(2);
        fri = Eigen::Vector2d::Zero(2);
        cmp = Eigen::Vector2f::Zero(2);
        joint = Eigen::VectorXf::Zero(ndof);
        joint_vel = Eigen::VectorXf::Zero(ndof);
        joint_acc = Eigen::VectorXf::Zero(ndof);
        sensor = Eigen::VectorXf::Zero(52*3);
        sensor_vel = Eigen::VectorXf::Zero(52*3);
        sensor_acc = Eigen::VectorXf::Zero(52*3);
        sensor_angle = Eigen::VectorXf::Zero(52*3);
        sensor_angle_local = Eigen::VectorXf::Zero(52*3);
        sensor_angle_acc_local = Eigen::VectorXf::Zero(52*3);
        zmpSupportPolygonRatio = 0.0f;
        zmpNecessitiesEntryPtr = boost::shared_ptr<MotionFrameZmpNecessities> (new MotionFrameZmpNecessities());
        cut = Eigen::Vector2f::Zero();
        centroid = Eigen::Vector2f::Zero();

    }

    MotionFrame::MotionFrame(const MotionFrame &inpSrc)
    {
        this->ndof = inpSrc.ndof;
        timestep = inpSrc.timestep;
        joint = inpSrc.joint;
        joint_vel = inpSrc.joint_vel;
        joint_acc = inpSrc.joint_acc;
        sensor = inpSrc.sensor;
        sensor_acc = inpSrc.sensor_acc;
        sensor_angle = inpSrc.sensor_angle;
        sensor_angle_acc_local = inpSrc.sensor_angle_acc_local;
        sensor_vel = inpSrc.sensor_vel;
        motionFrameEntries = inpSrc.motionFrameEntries;
        zmpNecessitiesEntryPtr = inpSrc.zmpNecessitiesEntryPtr;
        supportPolygon = inpSrc.supportPolygon;
        zmpSupportPolygonRatio = inpSrc.zmpSupportPolygonRatio;
        cut = inpSrc.cut;
        centroid = inpSrc.centroid;
    }

    std::string MotionFrame::toXML()
    {
        std::string tab1 = "\t\t\t";
        std::string tab2 = "\t\t\t\t";
        std::stringstream res;

        res << tab1 << "<MotionFrame>" << endl;
        res << tab2 << "<Timestep>" << timestep << "</Timestep>" << endl;
        std::map<std::string, MotionFrameEntryPtr>::iterator i = motionFrameEntries.begin();
        res << tab2 << "<ZmpPosition>"<< zmp[0]<< " " << zmp[1] << "</ZmpPosition>"<< endl;
        res << tab2 << "<FriPosition>"<< fri[0]<< " " << fri[1] << " " << "</FriPosition>"<< endl;
        res << tab2 << "<CmpPosition>"<< cmp[0]<< " " << cmp[1] << " " << "</CmpPosition>"<< endl;
        res << tab2 << "<stanceFoot>"<< stance_foot << "</stanceFoot>" << endl;
        if (supportPolygon.size()>0){
                res<< tab2 << "<Supportpolygon>";
            for( int j = 0; j<(int)supportPolygon.size();j++){
                res <<supportPolygon[j][0] << " " << supportPolygon[j][1] << endl; //scaled cm to Meter here
            }
           res << "</Supportpolygon>"<< endl;
        }
        res<< tab2 << "<ZmpSupportPolygonRatio>" << zmpSupportPolygonRatio << "</ZmpSupportPolygonRatio>"<< endl;
        res<< tab2 << "<Centroid>" << centroid[0] << " " << centroid[1] << "</Centroid>"<< endl;
        res<< tab2 << "<cut>" << cut[0] <<" "<<cut[1] << "</cut>"<< endl;
//        while (i != motionFrameEntries.end())
//        {
//            res << i->second->toXML();
//            i++;
//        }
//        if(joint.rows()>0)
//        {res << tab2 << "<JointPosition>";
//            for (int j=0;j<(int)joint.rows();j++)
//            {
//                res << joint[j] << " ";
//            }
//            res << "</JointPosition>" << endl;
//        }
//        if(joint_vel.rows() > 0)
//        {
//            res << tab2 << "<JointVelocity>";
//            for (int j=0;j<(int)joint_vel.rows();j++)
//            {
//                res << joint_vel[j] << " ";
//            }
//            res << "</JointVelocity>" << endl;
//        }
//        if(joint_acc.rows() > 0)
//        {
//            res << tab2<< "<JointAcceleration>";
//            for (int j=0;j<(int)joint_acc.rows();j++)
//            {
//                res << joint_acc[j] << " ";
//            }
//            res << "</JointAcceleration>" << endl;
//        }
        if(sensor.rows() > 0)
        {
            res << tab2<< "<SensorPosition>";
            for (int j=0;j<(int)sensor.rows();j++)
            {
                res << sensor[j] << " ";
            }
            res << "</SensorPosition>" << endl;
        }
        if(sensor_vel.rows() > 0)
        {
            res << tab2<< "<SensorVel>";
            for (int j=0;j<(int)sensor_vel.rows();j++)
            {
                res << sensor_vel[j] << " ";
            }
            res << "</SensorVel>" << endl;
        }
        if(sensor_acc.rows() > 0)
        {
            res << tab2<< "<SensorAcceleration>";
            for (int j=0;j<(int)sensor_acc.rows();j++)
            {
                res << sensor_acc[j] << " ";
            }
            res << "</SensorAcceleration>" << endl;
        }
        if(sensor_angle.rows() > 0)
        {
            res << tab2<< "<SensorOrientation>";
            for (int j=0;j<(int)sensor_angle.rows();j++)
            {
                res << sensor_angle[j] << " ";
            }
            res << "</SensorOrientation>" << endl;
        }
        if(sensor_angle_acc_local.rows() > 0)
        {
            res << tab2<< "<SensorAngularAcc>";
            for (int j=0;j<(int)sensor_angle_acc_local.rows();j++)
            {
                res << sensor_angle_acc_local[j] << " ";
            }
            res << "</SensorAngularAcc>" << endl;
        }
        res << tab1 << "</MotionFrame>" << endl;
        return res.str();

    }

    void MotionFrame::setStanceFoot(std::string stance)
    {
        stance_foot = stance;
    }


    bool MotionFrame::addEntry( const std::string &name, MotionFrameEntryPtr entry )
    {
        std::string lc = name;
        XML::toLowerCase(lc);
        motionFrameEntries[lc] = entry;
        return true;
    }

    MotionFrameZmpNecessitiesPtr MotionFrame::getZmpEntry(){
        return zmpNecessitiesEntryPtr ;
    }


    MMM::MotionFrameEntryPtr MotionFrame::getEntry( const std::string &name )
    {
        std::string lc = name;
        XML::toLowerCase(lc);
        if (!hasEntry(lc))
        {
            MMM_INFO << "Could not find entry with name:" << name << endl;
            return MMM::MotionFrameEntryPtr();
        }
        return motionFrameEntries[lc];
    }

    bool MotionFrame::hasEntry( const std::string &name ) const
    {
        std::string lc = name;
        XML::toLowerCase(lc);
        if (motionFrameEntries.find(lc) != motionFrameEntries.end())
        {
            return true;
        }
        return false;
    }

    bool MotionFrame::removeEntry( const std::string &name )
    {
        std::string lc = name;
        XML::toLowerCase(lc);
        if (hasEntry(lc))
            motionFrameEntries.erase(lc);
        return true;
    }

    Eigen::Vector3f MotionFrame::getRootPos()
    {
        if (!hasEntry("RootPosition"))
            return Eigen::Vector3f::Zero();
        MotionFrameEntryPtr e = getEntry("RootPosition");
        RootPositionPtr r = boost::dynamic_pointer_cast<RootPosition>(e);
        if (!r)
            return Eigen::Vector3f::Zero();
        return r->root_pos;
    }

    Eigen::Vector3f MotionFrame::getRootPosVel()
    {
        if (!hasEntry("RootPositionVelocity"))
            return Eigen::Vector3f::Zero();
        MotionFrameEntryPtr e = getEntry("RootPositionVelocity");
        RootPositionVelPtr r = boost::dynamic_pointer_cast<RootPositionVel>(e);
        if (!r)
            return Eigen::Vector3f::Zero();
        return r->root_pos_vel;
    }

    Eigen::Vector3f MotionFrame::getRootPosAcc()
    {
        if (!hasEntry("RootPositionAcceleration"))
            return Eigen::Vector3f::Zero();
        MotionFrameEntryPtr e = getEntry("RootPositionAcceleration");
        RootPositionAccPtr r = boost::dynamic_pointer_cast<RootPositionAcc>(e);
        if (!r)
            return Eigen::Vector3f::Zero();
        return r->root_pos_acc;
    }


    Eigen::Vector3f MotionFrame::getRootRot()
    {
        if (!hasEntry("RootRotation"))
            return Eigen::Vector3f::Zero();
        MotionFrameEntryPtr e = getEntry("RootRotation");
        RootOrientationPtr r = boost::dynamic_pointer_cast<RootOrientation>(e);
        if (!r)
            return Eigen::Vector3f::Zero();
        return r->root_ori;
    }

    Eigen::Vector3f MotionFrame::getRootRotVel()
    {
        if (!hasEntry("RootRotationVelocity"))
            return Eigen::Vector3f::Zero();
        MotionFrameEntryPtr e = getEntry("RootRotationVelocity");
        RootOrientationVelPtr r = boost::dynamic_pointer_cast<RootOrientationVel>(e);
        if (!r)
            return Eigen::Vector3f::Zero();
        return r->root_ori_vel;
    }

    Eigen::Vector3f MotionFrame::getRootRotAcc()
    {
        if (!hasEntry("RootRotationAcceleration"))
            return Eigen::Vector3f::Zero();
        MotionFrameEntryPtr e = getEntry("RootRotationAcceleration");
        RootOrientationAccPtr r = boost::dynamic_pointer_cast<RootOrientationAcc>(e);
        if (!r)
            return Eigen::Vector3f::Zero();
        return r->root_ori_acc;
    }

    Eigen::Matrix4f MotionFrame::getRootPose()
    {
        Eigen::Matrix4f result;
        result = Math::poseRPYToMatrix4f(getRootPos(),getRootRot());
        return result;
    }

    /*boost::shared_ptr<Eigen::Matrix4f> MotionFrame::rootPosePointer()
    {
        Eigen::Matrix4f* result = getRootPose();
        boost::shared_ptr<Eigen::Matrix4f> pose (result);
        return pose;
    }*/

    boost::shared_ptr<Eigen::Matrix4f> MotionFrame::rootPosePointer()
        {
            Eigen::Matrix4f result = getRootPose();
            //std::shared_ptr<Eigen::Matrix4f> pose(&result);
            //return *pose.get();
        //int foo = 4;
        //int* pose = (&foo);
        boost::shared_ptr<Eigen::Matrix4f> posePointer = boost::make_shared<Eigen::Matrix4f>(result);
        return posePointer;
        }

    bool MotionFrame::setRootPose(const Eigen::Matrix4f &pose)
    {
        Eigen::VectorXf p = Math::matrix4fToPoseRPY(pose);
        return (setRootPos(p.head(3)) && setRootRot(p.tail(3)));
    }

    bool MotionFrame::setRootPos(const Eigen::Vector3f &pos)
    {
        RootPositionPtr r(new RootPosition());
        r->root_pos = pos;
        return addEntry("RootPosition", r);
    }

    bool MotionFrame::setRootPosVel(const Eigen::Vector3f &posVel)
    {
        RootPositionVelPtr r(new RootPositionVel());
        r->root_pos_vel = posVel;
        return addEntry("RootPositionVelocity", r);
    }

    bool MotionFrame::setRootPosAcc(const Eigen::Vector3f &posAcc)
    {
        RootPositionAccPtr r(new RootPositionAcc());
        r->root_pos_acc = posAcc;
        return addEntry("RootPositionAcceleration", r);
    }

    bool MotionFrame::setRootRot(const Eigen::Vector3f &rot)
    {
        RootOrientationPtr r(new RootOrientation());
        r->root_ori = rot;
        return addEntry("RootRotation", r);
    }

    bool MotionFrame::setRootRotVel(const Eigen::Vector3f &rotVel)
    {
        RootOrientationVelPtr r(new RootOrientationVel());
        r->root_ori_vel = rotVel;
        return addEntry("RootRotationVelocity", r);
    }

    bool MotionFrame::setRootRotAcc(const Eigen::Vector3f &rotAcc)
    {
        RootOrientationAccPtr r(new RootOrientationAcc());
        r->root_ori_acc = rotAcc;
        return addEntry("RootRotationAcceleration", r);
    }
    bool MotionFrame::setSensorPositionAndEularian(Eigen::Matrix4f sensorValues, int pos,int global){
        if(global==0){

            Eigen::VectorXf sensorPosAndEularian = Math::matrix4fToPoseRPY(sensorValues*0.001);
            sensor_angle[3*pos]=sensorPosAndEularian[3];
            sensor_angle[3*pos +1]=sensorPosAndEularian[4];
            sensor_angle[3*pos +2]=sensorPosAndEularian[5];
        }
        else{
            Eigen::VectorXf sensorPosAndEularian = Math::matrix4fToPoseRPY(sensorValues*0.001);
            sensor[3*pos]= sensorPosAndEularian[0];
            sensor[3*pos +1]= sensorPosAndEularian[1];
            sensor[3*pos +2]= sensorPosAndEularian[2];
            sensor_angle_local[3*pos]=sensorPosAndEularian[3];
            sensor_angle_local[3*pos +1]=sensorPosAndEularian[4];
            sensor_angle_local[3*pos +2]=sensorPosAndEularian[5];
            return false;
        }
        return true;
    }

    bool MotionFrame::addSupportPolygon(std::vector<Eigen::Vector2f> points)
    {for(int i = 0; i<points.size(); i++){
            points[i] = 0.001*points[i];
        }
        points.push_back(points[0]);
        supportPolygon = points;
    }


}
