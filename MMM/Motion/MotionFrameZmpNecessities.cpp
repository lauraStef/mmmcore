#include "MotionFrameZmpNecessities.h"
#include "Motion.h"
#include "math.h"
namespace MMM{
float scaleToMeter = 0.001;

    MotionFrameZmpNecessities::MotionFrameZmpNecessities(){
        this->number_of_relevant_segments = 19;
        this->segment_COM = Eigen::VectorXf::Zero(3*number_of_relevant_segments);
        this->segment_COM_derivative = Eigen::VectorXf::Zero(3*number_of_relevant_segments);
        this->com = Eigen::Vector3f::Zero();
        this->angular_velocity_segments = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->angular_velocity_segments_derivative = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->inertia_tensor_list.resize(number_of_relevant_segments);
        this->angular_momentum =Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->segment_mass = Eigen::VectorXf::Zero(this->number_of_relevant_segments);
        this->angular_pos_segments = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->angular_pos_segments_derivative = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->angular_pos_segments_2nd_derivative = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
    }

    bool MotionFrameZmpNecessities::setNumberOfRelevantSegments(int seg_number){
        this->number_of_relevant_segments = seg_number;
        this->segment_COM = Eigen::VectorXf::Zero(3*number_of_relevant_segments);
        this->segment_COM_derivative = Eigen::VectorXf::Zero(3*number_of_relevant_segments);
        this->rotation_matrix_segments.resize(number_of_relevant_segments);
        this->inertia_tensor_list.resize(number_of_relevant_segments);
        this->angular_velocity_segments = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->angular_velocity_segments_derivative = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->angular_momentum =Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->segment_mass = Eigen::VectorXf::Zero(this->number_of_relevant_segments);
        this->angular_pos_segments = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->angular_pos_segments_derivative = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        this->angular_pos_segments_2nd_derivative = Eigen::VectorXf::Zero(3*this->number_of_relevant_segments);
        return true;
    }



    bool MotionFrameZmpNecessities::calculateAngularVel(Eigen::Matrix4f poseToRoot,int i)
    {
//            Eigen::Vector3f distanceToRoot = poseToRoot.rightCols<1>().head(3);
//            distanceToRoot = 0.001*distanceToRoot;
//            Eigen::Vector3f velocity(segment_COM_derivative[i*3],segment_COM_derivative[(i*3) +1],segment_COM_derivative[(i*3) + 2]);
//            Eigen::Vector3f normal = distanceToRoot/(std::sqrt(MMM::Math::dotProduct(distanceToRoot,distanceToRoot)));
//            Eigen::Vector3f tangentialComponent = velocity - (MMM::Math::dotProduct(distanceToRoot,normal)*normal);
//            Eigen::Vector3f angVel = ((distanceToRoot.cross(tangentialComponent))/MMM::Math::dotProduct(distanceToRoot,distanceToRoot));
////            Eigen::Vector3f angVel2 = angVel;
////            angVel = angVel2.cross(distanceToRoot);
//            angular_velocity_segments[3*i] = angVel[0];
//            angular_velocity_segments[3*i +1] = angVel[1];
//            angular_velocity_segments[3*i +2] = angVel[2];
//        return true;
        for(int i = 0; i< number_of_relevant_segments; i++){
            angular_velocity_segments[3*i] = angular_pos_segments_derivative[3*i +1]*cos(angular_pos_segments[3*i]) + angular_pos_segments_derivative[3*i + 2]*sin(angular_pos_segments[3*i +1])*sin(angular_pos_segments[3*i]);
            angular_velocity_segments[3*i +1] = angular_pos_segments_derivative[3*i +1]*sin(angular_pos_segments[3*i]) - angular_pos_segments_derivative[3*i + 2]*cos(angular_pos_segments[3*i])*sin(angular_pos_segments[3*i +1]);
            angular_velocity_segments[3*i +2] = angular_pos_segments_derivative[3*i] + angular_pos_segments_derivative[3*i +2]*cos(angular_pos_segments[3*i + 1]);

            angular_velocity_segments_derivative[3*i] = cos(angular_pos_segments[3*i])*(angular_pos_segments_2nd_derivative[3*i +1] + angular_pos_segments_derivative[3*i]*angular_pos_segments_derivative[3*i+2]*sin(angular_pos_segments[3*i +1 ])) + sin(angular_pos_segments[3*i])*(angular_pos_segments_2nd_derivative[3*i + 2]*sin(angular_pos_segments[3*i +1])+ angular_pos_segments_derivative[3*i + 1]*angular_pos_segments_derivative[3*i +2]*cos(angular_pos_segments[3*i +1]) -angular_pos_segments_derivative[3*i + 1]*angular_pos_segments_derivative[3*i]);
            angular_velocity_segments_derivative[3*i +1] = sin(angular_pos_segments[3*i])*(angular_pos_segments_2nd_derivative[3*i +1] + angular_pos_segments_derivative[3*i]*angular_pos_segments_derivative[3*i+2]*sin(angular_pos_segments[3*i +1 ])) + cos(angular_pos_segments[3*i])*(angular_pos_segments_derivative[3*i + 1]*angular_pos_segments_derivative[3*i]-angular_pos_segments_2nd_derivative[3*i + 2]*sin(angular_pos_segments[3*i +1]) - (angular_pos_segments_derivative[3*i +1]*angular_pos_segments_derivative[3*i + 2]*cos(angular_pos_segments[3*i +1])) );
            angular_velocity_segments_derivative[3*i +2] = angular_pos_segments_2nd_derivative[3*i] + angular_pos_segments_2nd_derivative[3*i +2]*cos(angular_pos_segments[3*i + 1]) -angular_pos_segments_derivative[3*i +1]*angular_pos_segments_derivative[3*i +2]*sin(angular_pos_segments[3*i+1]);
        }
    }

    bool MotionFrameZmpNecessities::setRotationMatrix(Eigen::Matrix4f rootPose, Eigen::Matrix4f rotations, int pos)
    {
            this->rotation_matrix_segments.push_back((rescaleToRootPose(rootPose)* rotations*scaleToMeter).block(0,0,3,3));
//        this->rotation_matrix_segments.push_back(( rotations*scaleToMeter).block(0,0,3,3));


        return true;
    }

    bool MotionFrameZmpNecessities::setCOM(Eigen::Vector3f com2, Eigen::Matrix4f globalPose){
        com = com2*scaleToMeter;
        return true;
    }
    void MotionFrameZmpNecessities::setSegmentMasses(std::vector<float> segment_masses){
        for(int i = 0; i< segment_masses.size(); i++){
            this->segment_mass[i] = segment_masses[i]*100.;
        }
    }


    void MotionFrameZmpNecessities::setAngularMomentum(std::vector<Eigen::Matrix3f> inertia_tensors)
    {
        for(int i = 0; i < inertia_tensors.size(); i++){
            Eigen::Vector3f ang_vel = Eigen::Vector3f(angular_velocity_segments[3*i],angular_velocity_segments[3*i+1],angular_velocity_segments[3*i+2]);//rotation_matrix_segments[i].transpose() *
            Eigen::Vector3f ang_vel_der = Eigen::Vector3f(angular_velocity_segments_derivative[3*i],angular_velocity_segments_derivative[3*i+1],angular_velocity_segments_derivative[3*i+2]);
            this->angular_momentum[3*i] = (rotation_matrix_segments[i] * inertia_tensors[i] * ang_vel)[0];
            this->angular_momentum[3*i+1] = (rotation_matrix_segments[i] * inertia_tensors[i] * ang_vel)[1];
            this->angular_momentum[3*i+2] = (rotation_matrix_segments[i] * inertia_tensors[i] * ang_vel)[2];

        }
    }

    bool MotionFrameZmpNecessities::setSegmentsCOM(Eigen::Matrix4f rootPose, Eigen::Vector3f com2, int segment){
        com2 = com2*scaleToMeter;
        Eigen::Vector4f com =  Eigen::Vector4f(com2[0],com2[1],com2[2],1.0);
        Eigen::VectorXf com_eigen_format = this -> segment_COM;
            com_eigen_format[segment*3] = com[0];
            com_eigen_format[segment*3 + 1] = com[1];
            com_eigen_format[segment*3 + 2] = com[2];
        this->segment_COM = com_eigen_format;
            return true;
    }

    void MotionFrameZmpNecessities::setAngularPos(Eigen::Matrix4f pose, int pos)
    {Eigen::VectorXf localPose = Math::matrix4fToPoseEulerXZY(pose*scaleToMeter);
        angular_pos_segments[3*pos]=localPose[3];
        angular_pos_segments[3*pos +1]=localPose[4];
        angular_pos_segments[3*pos +2]=localPose[5];

    }

    Eigen::Matrix4f MotionFrameZmpNecessities::rescaleToRootPose(Eigen::Matrix4f rootPose)
    {   Eigen::Matrix4f result = Eigen::Matrix4f::Zero();
        result.block(0,0,3,3) = rootPose.inverse().block(0,0,3,3);
        result.block(0,3,3,1) = -rootPose.block(0,3,3,1);
        return result;

    }

}
