#ifndef SPLINEFUNCTION_H
#define SPLINEFUNCTION_H


#include "splinefunction.h"
#include "alglib/src/interpolation.h"
#include "MMM/MMMCore.h"
#include <Eigen/Core>
#include <unsupported/Eigen/Splines>


namespace MMM{
class SplineFunction;

class MMM_IMPORT_EXPORT SplineFunction
{

public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
//    SplineFunction();
    SplineFunction(Eigen::VectorXf input);
    SplineFunction(Eigen::VectorXf input, int p);
    Eigen::VectorXf derivative;
    Eigen::VectorXf derivative2ndOrder;
//    Eigen::Spline<float,1> spline;
    typedef Eigen::Spline<float,1> spline;
    typedef spline::ControlPointVectorType ControlPointVectorType;


};

}
#endif // SPLINEFUNCTION_H
