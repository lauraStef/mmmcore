#include "MathTools.h"

#include <Eigen/Core>
#include <Eigen/Geometry>

using std::cout;
using std::endl;

namespace MMM
{
namespace Math
{


	Eigen::Matrix4f rpy2eigen4f (float r, float p, float y)
	{
		float salpha,calpha,sbeta,cbeta,sgamma,cgamma;

		sgamma = sinf(r);
		cgamma = cosf(r);
		sbeta = sinf(p);
		cbeta = cosf(p);
		salpha = sinf(y);
		calpha = cosf(y);

		Eigen::Matrix4f m;

		m(0,0) = (float)(calpha*cbeta);
		m(0,1) = (float)(calpha*sbeta*sgamma-salpha*cgamma);
		m(0,2) = (float)(calpha*sbeta*cgamma+salpha*sgamma);
		m(0,3) = 0;//x

		m(1,0) = (float)(salpha*cbeta);
		m(1,1) = (float)(salpha*sbeta*sgamma+calpha*cgamma);
		m(1,2) = (float)(salpha*sbeta*cgamma-calpha*sgamma);
		m(1,3) = 0;//y

		m(2,0) = (float)-sbeta;
		m(2,1) = (float)(cbeta*sgamma);
		m(2,2) = (float)(cbeta*cgamma);
		m(2,3) = 0;//z

		m(3,0) = 0;
		m(3,1) = 0;
		m(3,2) = 0;
		m(3,3) = 1.0f;

		return m;
	}

	Eigen::Matrix4f quat2eigen4f( float x, float y, float z, float w )
	{
		Eigen::Matrix4f m;
		m.setIdentity();
		Eigen::Quaternionf q(w,x,y,z);
		Eigen::Matrix3f m3;
		m3 = q.toRotationMatrix();
		m.block(0,0,3,3) = m3;
		return m;
	}

	Eigen::Vector3f matrix3fToEulerXZY(const Eigen::Matrix3f &m)
	{
        Eigen::Vector3f rotEuler = m.eulerAngles(0, 2, 1);
		return rotEuler;
	}

	Eigen::Vector3f matrix3fToRPY(const Eigen::Matrix3f &m)
	{
		//Eigen::Matrix4f A = mat.transpose();
		float alpha, beta, gamma;
		beta = atan2(-m(2, 0), sqrtf(m(0, 0)*m(0, 0) + m(1, 0)*m(1, 0)));
		if (fabs(beta - (float)M_PI*0.5f) < 1e-10)
		{
			alpha = 0;
			gamma = atan2(m(0, 1), m(1, 1));
		}
		else if (fabs(beta + (float)M_PI*0.5f) < 1e-10)
		{
			alpha = 0;
			gamma = -atan2(m(0, 1), m(1, 1));
		}
		else
		{
			float cb = 1.0f / cosf(beta);
			alpha = atan2(m(1, 0)*cb, m(0, 0)*cb);
			gamma = atan2(m(2, 1)*cb, m(2, 2)*cb);
		}
		Eigen::Vector3f res(gamma, beta, alpha);
		return res;
	}

	Eigen::Vector3f matrix4fToEulerXZY( const Eigen::Matrix4f &m) 
	{
		return matrix3fToEulerXZY(m.block(0,0,3,3));
	}

	Eigen::VectorXf matrix4fToPoseEulerXZY(const Eigen::Matrix4f &m)
	{
		Eigen::VectorXf r(6);
		r.head(3) = m.block(0, 3, 3, 1);
		r.tail(3) = matrix3fToEulerXZY(m.block(0, 0, 3, 3));
		return r;
	}

	Eigen::VectorXf matrix4fToPoseRPY(const Eigen::Matrix4f &m)
	{
		Eigen::VectorXf r(6);
		r.head(3) = m.block(0, 3, 3, 1);
		r.tail(3) = matrix3fToRPY(m.block(0, 0, 3, 3));
		return r;
	}

	Eigen::Matrix3f eulerXZYToMatrix3f( const Eigen::Vector3f &eulerXZY )
	{
		Eigen::Matrix3f m_3;
		m_3 =  Eigen::AngleAxisf(eulerXZY[0], Eigen::Vector3f::UnitX())
			* Eigen::AngleAxisf(eulerXZY[1], Eigen::Vector3f::UnitZ())
			* Eigen::AngleAxisf(eulerXZY[2], Eigen::Vector3f::UnitY());
		return m_3;
	}

	Eigen::Matrix4f poseEulerXZYToMatrix4f( const Eigen::Vector3f &pos, const Eigen::Vector3f &eulerXZY )
	{
		Eigen::Matrix4f m;
		m.setIdentity();
		m.block(0,3,3,1) = pos;

		m.block(0,0,3,3) = eulerXZYToMatrix3f(eulerXZY);
		return m;
	}

	Eigen::Matrix4f poseRPYToMatrix4f( const Eigen::Vector3f &pos, const Eigen::Vector3f &rpy )
	{
		Eigen::Matrix4f m;
		m.setIdentity();
		m = rpy2eigen4f(rpy(0),rpy(1),rpy(2));
		m.block(0,3,3,1) = pos;
        return m;
    }

    Eigen::Vector2f centroid(std::vector<Eigen::Vector2f> convexHull)
    {if(convexHull.size()>2){
        float area = 0.0f;
        for (int i = 0; i<convexHull.size() - 1; i++){
            area+= 0.5*(convexHull[i][0]*convexHull[i+1][1]-convexHull[i+1][0]*convexHull[i][1]);
        }
        float x = 0.0f;
        for (int i = 0; i<convexHull.size() -1; i++){
            x += (1/(6*area))*((convexHull[i][0]+convexHull[i+1][0])*(convexHull[i][0]*convexHull[i+1][1] - convexHull[i+1][0]*convexHull[i][1]));
        }
        float y = 0.0f;
        for (int i = 0; i<convexHull.size() -1; i++){
            y += (1/(6*area))*((convexHull[i][1]+convexHull[i+1][1])*(convexHull[i][0]*convexHull[i+1][1] - convexHull[i+1][0]*convexHull[i][1]));
        }
        return Eigen::Vector2f(x,y);}
        else return Eigen::Vector2f(-1000.0,-1000.0);

    }
    float dotProduct(Eigen::Vector3f firstVec, Eigen::Vector3f secondVec)
    {
        return firstVec[0] * secondVec[0] + firstVec[1]* secondVec[1] + firstVec[2] * secondVec[2];
    }
    float dotProduct(Eigen::Vector2f firstVec, Eigen::Vector2f secondVec)
    {
        return firstVec[0] * secondVec[0] + firstVec[1]* secondVec[1];
    }

    bool checkSide(Eigen::Vector2f supportPolygon, Eigen::Vector2f centroid, Eigen::Vector2f zmp)
    {
        return ((((centroid[0]-zmp[0])*(supportPolygon[1]- zmp[1]))-((centroid[1]-zmp[1])*(supportPolygon[0]-zmp[0])))>0);

    }

    float distance(Eigen::Vector2f p1, Eigen::Vector2f p2)
    {
        return std::sqrt(std::pow(p1[0]-p2[0],2.0) + std::pow(p1[1] - p2[1],2.0)) ;

    }

    Eigen::Vector2f findIntersection(Eigen::Vector2f centroid, Eigen::Vector2f zmp, std::vector<Eigen::Vector2f> supportPolygon)
    {
        //     THROW_VR_EXCEPTION_IF(supportPolygon.size() < 1, "Could not generate convex hull with " << n << " points...");
        if(supportPolygon.size()>0){
            bool right = checkSide(supportPolygon[0], centroid, zmp);
            bool setFirst = false;
            Eigen::Vector2f firstPointLine, secondPointLine, firstPointLine2, secondPointLine2  = Eigen::Vector2f(0.0f,0.0f);
            for (int i = 1; i<supportPolygon.size(); i++){
                // Check wether succeeding points are on different sides of the line through zmp and centroid
                if(!(!right && !checkSide(supportPolygon[i],centroid,zmp))&& !(right && checkSide(supportPolygon[i],centroid,zmp))){
                    if(!setFirst){
                        firstPointLine = supportPolygon[i-1];
                        secondPointLine = supportPolygon[i];
                        right = !right;
                        setFirst = true;
                    }
                    else{
                        firstPointLine2 = supportPolygon[i-1];
                        secondPointLine2 = supportPolygon[i];

                        break;
                    }
                }
            }
            //calculate x and y coordinate of intersection of the line through zmp and centroid and the supportPolygon
            float x = ((centroid[0]*zmp[1] -centroid[1]*zmp[0])*(firstPointLine[0]-secondPointLine[0])-(centroid[0]-zmp[0])*(firstPointLine[0]*secondPointLine[1]-firstPointLine[1]*secondPointLine[0]))/((centroid[0]-zmp[0])*(firstPointLine[1]-secondPointLine[1])-(centroid[1]-zmp[1])*(firstPointLine[0]-secondPointLine[0]));
            float y = ((centroid[0]*zmp[1] -centroid[1]*zmp[0])*(firstPointLine[1]-secondPointLine[1])-(centroid[1]-zmp[1])*(firstPointLine[0]*secondPointLine[1]-firstPointLine[1]*secondPointLine[0]))/((centroid[0]-zmp[0])*(firstPointLine[1]-secondPointLine[1])-(centroid[1]-zmp[1])*(firstPointLine[0]-secondPointLine[0]));

            float x2 = ((centroid[0]*zmp[1] -centroid[1]*zmp[0])*(firstPointLine2[0]-secondPointLine2[0])-(centroid[0]-zmp[0])*(firstPointLine2[0]*secondPointLine2[1]-firstPointLine2[1]*secondPointLine2[0]))/((centroid[0]-zmp[0])*(firstPointLine2[1]-secondPointLine2[1])-(centroid[1]-zmp[1])*(firstPointLine2[0]-secondPointLine2[0]));
            float y2 = ((centroid[0]*zmp[1] -centroid[1]*zmp[0])*(firstPointLine2[1]-secondPointLine2[1])-(centroid[1]-zmp[1])*(firstPointLine2[0]*secondPointLine2[1]-firstPointLine2[1]*secondPointLine2[0]))/((centroid[0]-zmp[0])*(firstPointLine2[1]-secondPointLine2[1])-(centroid[1]-zmp[1])*(firstPointLine2[0]-secondPointLine2[0]));
            //check on both points which on is at the same side than the zmp, seen from the centroid
            if (distance(zmp,Eigen::Vector2f(x,y)) <distance(zmp, Eigen::Vector2f(x2,y2))){
                return Eigen::Vector2f(x,y);
            }


            return Eigen::Vector2f(x2,y2);
        }

        else{
            return Eigen::Vector2f(zmp[0]+1000, zmp[1]+1000);
        }
    }
}

}


